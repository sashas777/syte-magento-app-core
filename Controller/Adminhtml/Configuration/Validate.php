<?php

/**
 * Syte_Core
 */

declare(strict_types=1);

namespace Syte\Core\Controller\Adminhtml\Configuration;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Syte\Core\Model\Events\Tracker;

class Validate extends Action
{
    /** @const string */
    public const ADMIN_RESOURCE = 'Magento_Config::config';

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var Tracker
     */
    private $tracker;

    /**
     * Validate constructor
     *
     * @param Action\Context $context
     * @param SerializerInterface $serializer
     * @param Tracker $tracker
     */
    public function __construct(
        Action\Context $context,
        SerializerInterface $serializer,
        Tracker $tracker
    ) {
        parent::__construct($context);
        $this->serializer = $serializer;
        $this->tracker = $tracker;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $data = $this->serializer->unserialize($this->getRequest()->getParam('endpoint_data'));
        $data['account_id'] = $this->getRequest()->getParam('account_id');
        $data['sig'] = $this->getRequest()->getParam('sig');
        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        try {
            $status = $this->tracker->validate($data);
            $response->setHttpResponseCode(200);
            $response->setData([
                'responce_code' => $status,
                'validated' => ($status >= 200 && $status < 300)
            ]);
        } catch (\Exception $e) {
            $response->setHttpResponseCode(500);
        }

        return $response;
    }
}
