require([
        'jquery',
        'Magento_Ui/js/modal/alert',
        'mage/translate',
        'jquery/validate'
        ], function ($, alert, $t) {

            const syte_account_id_input = '[data-ui-id="text-groups-account-fields-id-value"]';
            const syte_account_signature_input = '[data-ui-id="text-groups-account-fields-signature-value"]';
            const syte_validation_obj = '[data-ui-id="text-groups-account-fields-validated-value"]';
            const syte_validation_msg = 'syte-credentials-success-message';
            const syte_validation_btn = '.syte-validation-button';

            let setValidateStatus = function (isValidated) {
                let obj = $(syte_validation_obj);
                let btn = $(syte_validation_btn);
                if (typeof(isValidated) != 'undefined' && isValidated !== null) {
                    obj.val(isValidated ? '1' : '0');
                }
                if ($('.' + syte_validation_msg)) {
                    $('.' + syte_validation_msg).remove();
                }
                if (parseInt(obj.val()) == 1) {
                    $('<div class="message message-success ' + syte_validation_msg + '">' + $t("Your credentials are valid") + '</div>').insertAfter(btn);
                } else {
                    $('<div class="message message-warning ' + syte_validation_msg + '">' + $t("Your credentials must be validated") + '</div>').insertAfter(btn);
                }
            }

    let showValidationAlert = function () {
        setValidateStatus(false);
        alert({
            title: $t('Syte Credential Validation Failed'),
            content: $t('Your Syte Credentials could not be validated. Please ensure you have entered a valid Account ID and Account Signature.')
        });
    }

    window.syteValidator = function (endpoint, endpointData) {
        let validator = $('#config-edit-form').validate();
        /* Remove previous success message if present */
        if ($('.' + syte_validation_msg)) {
            $('.' + syte_validation_msg).remove();
        }
        /* Basic field validation */
        let errors = [];
        if (!validator.element($(syte_account_id_input))) {
            errors.push($t('Please enter valid Account ID'));
        }
        if (!validator.element($(syte_account_signature_input))) {
            errors.push($t('Please enter valid Account Signature'));
        }
        if (errors.length > 0) {
            alert({
                title: $t('Syte Credential Validation Failed'),
                content:  errors.join('<br />')
            });
            setValidateStatus(false);
            return false;
        }
        $(this).text($t("We're validating your credentials...")).attr('disabled', true);
        let self = this;
        $.post(endpoint, {
            account_id: $(syte_account_id_input).val(),
            sig: $(syte_account_signature_input).val(),
            endpoint_data: endpointData
        }).done(function (response, textStatus, jqXHR) {
            if ((textStatus === 'success')
                && (typeof(response) != "undefined")
                && (response !== null)
                && (response.validated === true)) {
                setValidateStatus(true);
            } else {
                showValidationAlert();
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            showValidationAlert();
        }).always(function () {
            $(self).text($t("Validate Credentials")).attr('disabled', false);
        });
    }

    $(document).ready(function () {
        setValidateStatus();
        $(syte_account_id_input).bind('change paste keyup', function (e) {
            setValidateStatus(false);
        });
        $(syte_account_signature_input).bind('change paste keyup', function (e) {
            setValidateStatus(false);
        });
    });
        });
