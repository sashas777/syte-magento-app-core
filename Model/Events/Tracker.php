<?php
declare(strict_types=1);

namespace Syte\Core\Model\Events;

use Zend_Http_Client;
use Syte\Core\Model\Http\Client;
use Syte\Core\Model\Config;
use Magento\Framework\Serialize\Serializer\Json;

class Tracker
{
    /**
     * @var Json
     */
    private $json;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var Config
     */
    private $config;

    /**
     * Tracker constructor
     *
     * @param Json $json
     * @param Client $httpClient
     * @param Config $config
     */
    public function __construct(
        Json $json,
        Client $httpClient,
        Config $config
    ) {
        $this->json = $json;
        $this->httpClient = $httpClient;
        $this->config = $config;
    }

    /**
     * Validate request for account_id and sig validation
     *
     * @param array $data
     *
     * @return int
     */
    public function validate(array $data): int
    {
        return $this->httpClient->sendRequest($data, Zend_Http_Client::GET);
    }

    /**
     * Send request on feed_writing_start event
     *
     * @param string $feedName
     * @param int|string|null $storeId
     */
    public function feedCreationStart(string $feedName, $storeId = null)
    {
        if ($this->config->isAccountActive($storeId)) {
            $rawData = [
                'raw_data' => sprintf("{feed:%s}", $feedName),
            ];

            $data = $this->buildRequestParamsData('feed_writing_start', $rawData, $storeId);
            $this->httpClient->sendRequest($data, Zend_Http_Client::POST);
        }
    }

    /**
     * Send request on feed_writing_end event
     *
     * @param string $feedName
     * @param int|string|null $storeId
     */
    public function feedCreationCompleted(string $feedName, $storeId = null)
    {
        if ($this->config->isAccountActive($storeId)) {
            $rawData = [
                'raw_data' => sprintf("{feed:%s}", $feedName),
            ];

            $data = $this->buildRequestParamsData('feed_writing_end', $rawData, $storeId);
            $this->httpClient->sendRequest($data, Zend_Http_Client::POST);
        }
    }

    /**
     * Send request on feed_writing_failed event
     *
     * @param string $feedName
     * @param string $errorMessage
     * @param int|string|null $storeId
     */
    public function feedCreationFailed(string $feedName, string $errorMessage, $storeId = null)
    {
        if ($this->config->isAccountActive($storeId)) {
            $rawData = [
                'raw_data' => sprintf("{feed:%s,error:%s}", $feedName, $errorMessage),
            ];

            $data = $this->buildRequestParamsData('feed_writing_failed', $rawData, $storeId);
            $this->httpClient->sendRequest($data, Zend_Http_Client::POST);
        }
    }

    /**
     * Send request on feed_upload_start event
     *
     * @param string $feedName
     * @param int|string|null $storeId
     */
    public function feedUploadStart(string $feedName, $storeId = null)
    {
        if ($this->config->isAccountActive($storeId)) {
            $rawData = [
                'raw_data' => sprintf("{feed:%s}", $feedName),
            ];

            $data = $this->buildRequestParamsData('feed_upload_start', $rawData, $storeId);
            $this->httpClient->sendRequest($data, Zend_Http_Client::POST);
        }
    }

    /**
     * Send request on feed_upload_end event
     *
     * @param string $feedName
     * @param int|string|null $storeId
     */
    public function feedUploadCompleted(string $feedName, $storeId = null)
    {
        if ($this->config->isAccountActive($storeId)) {
            $rawData = [
                'raw_data' => sprintf("{feed:%s}", $feedName),
            ];

            $data = $this->buildRequestParamsData('feed_upload_end', $rawData, $storeId);
            $this->httpClient->sendRequest($data, Zend_Http_Client::POST);
        }
    }

    /**
     * Send request on feed_upload_failed event
     *
     * @param string $feedName
     * @param string $errorMessage
     * @param int|string|null $storeId
     */
    public function feedUploadFailed(string $feedName, string $errorMessage, $storeId = null)
    {
        if ($this->config->isAccountActive($storeId)) {
            $rawData = [
                'raw_data' => sprintf("{feed:%s,error:%s}", $feedName, $errorMessage),
            ];

            $data = $this->buildRequestParamsData('feed_upload_failed', $rawData, $storeId);
            $this->httpClient->sendRequest($data, Zend_Http_Client::POST);
        }
    }

    /**
     * Send request on account_configured event
     *
     * @param array $configuration
     * @param int|string|null $storeId
     */
    public function configuration(array $configuration, $storeId = null)
    {
        $rawData = [
            'raw_data' => $configuration,
        ];

        $data = $this->buildRequestParamsData('account_configured', $rawData, $storeId);
        $this->httpClient->sendRequest($data, Zend_Http_Client::POST);
    }

    /**
     * Send request on configuration_modified event
     *
     * @param array $configuration
     * @param int|string|null $storeId
     */
    public function configurationModified(array $configuration, $storeId = null)
    {
        $rawData = [
            'raw_data' => $configuration,
        ];

        $data = $this->buildRequestParamsData('configuration_modified', $rawData, $storeId);
        $this->httpClient->sendRequest($data, Zend_Http_Client::POST);
    }

    /**
     * Build request params data array
     *
     * @param string $event
     * @param array $rawData
     * @param int|string|null $storeId
     *
     * @return array
     */
    private function buildRequestParamsData(string $event, array $rawData, $storeId = null): array
    {
        return [
            'tags' => 'syte_magento',
            'name' => $event,
            'account_id' => $this->config->getAccountId($storeId),
            'sig' => $this->config->getAccountSignature($storeId),
            'raw_data' => $this->json->serialize($rawData),
        ];
    }
}
