<?php

/**
 * Syte_Core
 */

declare(strict_types=1);

namespace Syte\Core\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class FirstConfig extends Field
{
    /**
     * @inheritDoc
     */
    protected function _renderScopeLabel(AbstractElement $element): string
    {
        // Return empty label
        return '';
    }

    /**
     * @inheritDoc
     */
    protected function _renderValue(AbstractElement $element)
    {
        // Hide empty first_config value
        $html = '<td class="value" style="display: none;">';
        $html .= $this->_getElementHtml($element);
        $html .= '</td>';

        return $html;
    }
}
