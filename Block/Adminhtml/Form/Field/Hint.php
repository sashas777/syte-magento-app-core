<?php

/**
 * Syte_Core
 */

declare(strict_types=1);

namespace Syte\Core\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Syte\Core\Model\Constants;

class Hint extends Field
{
    /**
     * @inheritDoc
     */
    protected function _renderScopeLabel(AbstractElement $element): string
    {
        // Return empty label
        return '';
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        $label = __('Please use predefined anchors for configurable parameters');
        $title = __('Anchors allowed:');
        $accountId = Constants::SYTE_SCRIPT_ANCHOR_ACCOUNT_ID . ' = ' . __('Accoint ID');
        $accountSignature = Constants::SYTE_SCRIPT_ANCHOR_ACCOUNT_SIG . ' = ' . __('Accoint Signature');
        $langCode = Constants::SYTE_SCRIPT_ANCHOR_LANG_CODE . ' = ' . __('Language Code (xx_XX)');
        $feedName = Constants::SYTE_SCRIPT_ANCHOR_FEED_NAME . ' = ' . __('Product Feed Name');
        // @codingStandardsIgnoreStart
        $html = <<<TEXT
        <span class="syte-script-anchor-hint">{$label}</span>
        <img class="syte-script-anchor-hint-img" src="{$this->getViewFileUrl('Syte_Core::images/info.png')}" 
        	title="{$title}&#10;{$accountId}&#10;{$accountSignature}&#10;{$langCode}&#10;{$feedName}">
TEXT;
        // @codingStandardsIgnoreEnd

        return $html;
    }
}
