<?php

/**
 * Syte_Core
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Syte_Core', __DIR__);
